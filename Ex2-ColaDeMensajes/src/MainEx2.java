import java.util.Random;

public class MainEx2 {
    private final static int QUEUE_SIZE = 5000;
    private final static int MAX_WRITTEN_MESSAGES = 10;
    private final static int NUM_READERS = 3;
    private final static int NUM_WRITERS = 5;
    private final static int THREAD_POOL = NUM_READERS + NUM_WRITERS;

    public static void main(String[] args) {

        MessageQueue messageQueue = new MessageQueue(MAX_WRITTEN_MESSAGES);
        Random random = new Random();

        Thread[] threadsArray = new Thread[THREAD_POOL];
        for (int i = 0; i < threadsArray.length; i++) {
            if (i < NUM_WRITERS) {
                threadsArray[i] = new Thread(new ThreadWriter(messageQueue,
                        "Message " + i + " " + (random.nextInt(QUEUE_SIZE) + 1)),
                        "Writer " + i);
            } else {
                threadsArray[i] = new Thread(new ThreadReader(messageQueue),
                        "Reader " + i);
            }
            threadsArray[i].start();
        }

        for (Thread thread : threadsArray) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("¡Hasta luego!");
    }
}
