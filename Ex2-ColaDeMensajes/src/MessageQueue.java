import java.util.ArrayList;

public class MessageQueue {
    private final int MAX_WRITTEN_MESSAGES;
    private final ArrayList<String> messagesArray;

    public MessageQueue(int MAX_WRITTEN_MESSAGES) {
        super();
        this.MAX_WRITTEN_MESSAGES = MAX_WRITTEN_MESSAGES;
        this.messagesArray = new ArrayList<>(MAX_WRITTEN_MESSAGES);
    }

    synchronized void putMessage(String message) {
        while (messagesArray.size() == MAX_WRITTEN_MESSAGES) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        messagesArray.add(message);
        notifyAll();
    }

    synchronized String getMessage() {
        while (messagesArray.size() == 0) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        String message = messagesArray.get(0);
        messagesArray.remove(0);
        notifyAll();
        return message;
    }
}
