public class ThreadWriter implements Runnable {

    private MessageQueue messageQueue;
    private String mensaje;

    public ThreadWriter(MessageQueue messageQueue, String mensaje) {
        super();
        this.messageQueue = messageQueue;
        this.mensaje = mensaje;
    }

    @Override
    public void run() {
        while (true) {
            messageQueue.putMessage(mensaje);
            System.out.println("Written: " + mensaje);
        }
    }
}

