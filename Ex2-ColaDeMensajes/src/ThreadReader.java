public class ThreadReader implements Runnable {

    private MessageQueue messageQueue;

    public ThreadReader(MessageQueue messageQueue) {
        super();
        this.messageQueue = messageQueue;
    }

    @Override
    public void run() {
        while (true) {
            System.out.println("Read: " + messageQueue.getMessage());
        }
    }
}