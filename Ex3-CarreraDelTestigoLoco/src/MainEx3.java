import java.util.Random;

public class MainEx3 {
    public static final int THREAD_POOL = 10;

    public static void main(String[] args) {
        ThreadJudge threadJudge = new ThreadJudge();
        Thread[] threadRunners = new Thread[THREAD_POOL];

        System.out.println("All runners threads created and waiting for the baton");
        threadJudge.countDown();

        Random random = new Random();
        for (int i = 0; i < threadRunners.length; i++) {
            threadRunners[i] = new Thread(new ThreadRunner("Runner " + i, threadJudge, random.nextInt(5-2)+2), "Runner " + i);
            threadRunners[i].start();
        }

        System.out.println("Judge throw the baton in the air. Who will take it?" + "\n");
        threadJudge.throwBaton();

        for (int i = 0; i < threadRunners.length; i++) {
            Thread thread = threadRunners[i];
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Judge says the crazy relay race is finished");
        System.out.println(threadJudge.getRaceResult());
    }
}
