public class ThreadJudge extends Thread implements Runnable {

    private boolean batonAvailable;
    private String arriveOrder;

    public ThreadJudge() {
        super();
        this.batonAvailable = false;
        this.arriveOrder = "The arrive order is:";
    }

    synchronized void catchBaton(String name) {
        while (!batonAvailable) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        arriveOrder += " " + name;
        batonAvailable = false;
    }

    void countDown() {
        try {
            System.out.println("Judge takes the baton and waits 5 seconds...");
            sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    synchronized void throwBaton() {
        batonAvailable = true;
        notifyAll();
    }

    public String getRaceResult() {
        return arriveOrder;
    }
}
