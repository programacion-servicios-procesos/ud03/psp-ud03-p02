public class ThreadRunner extends Thread implements Runnable {

    private final ThreadJudge threadJudge;
    private final String name;
    private final int sleep;

    public ThreadRunner(String name, ThreadJudge threadJudge, int sleep) {
        super();
        this.name = name;
        this.threadJudge = threadJudge;
        this.sleep = sleep;
    }

    @Override
    public void run() {
        threadJudge.catchBaton(name);
        System.out.println(name + " takes the baton and runs");

        try {
            Thread.sleep(sleep);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            threadJudge.throwBaton();
            System.out.println(name + " ran for " + sleep + " seconds and throws the baton in the air" + "\n");
        }
    }
}
