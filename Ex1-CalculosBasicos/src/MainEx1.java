import java.util.Scanner;

public class MainEx1 {

    static int increment = 0;
    static int numThreads;
    static Calculate calculate = new Calculate();

    public static void main(String[] args) {
        boolean correctInput = false;
        boolean correctInteger = false;

        /* Comprobar valor a calcular correcto */
        do {
            System.out.println("Introduce el valor a calcular");
            Scanner sc = new Scanner(System.in);

            if (sc.hasNextInt()) {
                increment = sc.nextInt();

                if (increment >= 1) {
                    correctInteger = true;
                } else {
                    System.out.println("Por favor introduce un número mayor que 0");
                }
            } else System.out.println("Por favor introduce un número mayor que 0");
        } while (!correctInteger);

        /* Comprobar numero de threads correcto */
        do {
            System.out.println("¿Cuantos hilos quieres ejecutar? Selecciona entre 50 y 100");
            Scanner sc = new Scanner(System.in);

            if (sc.hasNextInt()) {
                numThreads = sc.nextInt();

                if (numThreads >= 50 && numThreads <= 100) {
                    createThreads(numThreads, increment);
                    correctInput = true;
                } else {
                    System.out.println("Por favor introduce un número entre 50 y 100");
                }
            } else System.out.println("Por favor introduce un número entre 50 y 100");
        } while (!correctInput);
    }

    /* createThreads: Crea y lanza los threads */
    public static void createThreads(int numThreads, int increment) {
        for (int i = 0; i < numThreads; i++) {
            Thread thread = new Thread(new CalculateThread(calculate, increment));
            thread.start();
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

