public class Calculate {

    private int totalSumValue;
    private int averageValue;
    private int totalSumNumbers;

    public Calculate() {
        super();
        this.totalSumValue = 0;
        this.averageValue = 0;
        this.totalSumNumbers = 0;

    }

    public synchronized void newValue(int increment) {
        totalSumValue += increment;
        totalSumNumbers++;
        averageValue = totalSumValue / totalSumNumbers;
    }

    @Override
    public String toString() {
        return "Suma total de valores = " + totalSumValue + ", y el promedio es =  " + averageValue;
    }
}

