public class CalculateThread implements Runnable {

    Calculate calculate;
    private int increment;

    public CalculateThread(Calculate calculate, int increment) {
        super();
        this.calculate = calculate;
        this.increment = increment;
    }

    @Override
    public void run() {
        calculate.newValue(increment);
        System.out.println(calculate.toString());
    }
}
